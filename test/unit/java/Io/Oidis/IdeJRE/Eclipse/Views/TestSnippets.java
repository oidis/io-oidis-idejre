/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Eclipse.Views;

import Io.Oidis.IdeJRE.Interfaces.IPartFactory;
import Io.Oidis.IdeJRE.Interfaces.IPluginRuntime;
import Io.Oidis.IdeJRE.Loader;
import Io.Oidis.Jcommons.Resolvers.LiveContentWrapper;
import Io.Oidis.Jcommons.Resolvers.Protocols.LiveContentProtocol;
import Io.Oidis.UnitTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.util.List;

public class TestSnippets extends UnitTest {

    private static Logger logger = LogManager.getLogger(TestSnippets.class);

    public void testSnippet() throws NoSuchFieldException, IllegalAccessException {

        Field field = Loader.class.getDeclaredField("pluginRuntime");
        field.setAccessible(true);
        field.set(null, new IPluginRuntime() {
            @Override
            public void linkClientWithProject(int $clientId, String $projectId) {

            }

            @Override
            public String getProjectId(int $clientId) {
                return null;
            }

            @Override
            public String getWorkspaceRoot(int $clientId) {
                return null;
            }

            @Override
            public String getPluginId() {
                return null;
            }

            @Override
            public boolean openInfoDialog(String $title, String $message, int $clientId) {
                return false;
            }

            @Override
            public List<String> openFileChooser(String $path, String[] $extensions, int $clientId) {
                return null;
            }

            @Override
            public IPartFactory getPartFactory(String $projectId) {
                return null;
            }
        });

        LiveContentProtocol liveContentProtocol = new LiveContentProtocol();
        liveContentProtocol.name = "Io.Oidis.Jcommons.TestClass.TestMethod";
        liveContentProtocol.args = "[null]";
        // liveContentProtocol.args = "[\"C:/\", [\".jar\"]]";
        System.out.println(LiveContentWrapper.InvokeMethod(liveContentProtocol).returnValue);
    }

    public void testLogLevel() {
        logger.trace("LOGGER TEST");
        logger.debug("LOGGER TEST");
        logger.info("LOGGER TEST");
        logger.warn("LOGGER TEST");
        logger.error("LOGGER TEST");
        logger.fatal("LOGGER TEST");
    }
}
