/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Eclipse.Views;

import Io.Oidis.IdeJRE.Interfaces.IView;

import javax.swing.JComponent;

public class View implements IView {

    private final String id;

    public View(final String $id) {
        id = $id;
    }

    @Override
    public void setContent(final JComponent $content) {
        ViewContentPool.registerViewContent(id, $content);
    }
}
