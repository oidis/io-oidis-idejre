/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Components;

import Io.Oidis.IdeJRE.Components.Errors.NativeError;

public class HostLinux extends Host {
    public HostLinux(final String $windowIdentification) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    private HostLinux() {
    }

    @Override
    public void release() throws NativeError {
        throw new UnsupportedOperationException();
    }
}
