/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Helpers;

public final class Constants {
    private Constants() {
    }

    /**
     * Relative or absolute path the microcore where DLL and EXE reside.
     */
    public static final String microcorePath = "c:/burda/projects/wui/com-wui-framework-microcore/build-64/src/Release/";

    /**
     * Executable name.
     */
    public static final String microcoreExecutableName = "microcore-runner.exe";

    /**
     * Shared library name.
     */
    public static final String microcoreLibraryName = "microcore.dll";
}
