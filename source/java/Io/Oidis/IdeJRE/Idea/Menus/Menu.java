/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Idea.Menus;

import Io.Oidis.IdeJRE.Idea.Actions.ActionGroup;
import Io.Oidis.IdeJRE.Interfaces.IItem;
import Io.Oidis.IdeJRE.Interfaces.IMenu;

import java.util.Arrays;

public class Menu extends BaseItem implements IMenu {


    public Menu(final ActionGroup $nativeObject, final String $elementId) {
        super($nativeObject, $elementId);
    }

    @Override
    public void addItem(final IItem $item) {
        if ($item instanceof BaseItem) {
            BaseItem baseItem = (BaseItem) $item;
            ActionGroup nativeAction = ((ActionGroup) this.getNativeObject());
            if (!Arrays.asList(nativeAction.getChildActionsOrStubs()).contains(baseItem.getNativeObject())) {
                nativeAction.add(((BaseItem) $item).getNativeObject());
            }
        }
    }
}
