/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Services;

import Io.Oidis.IdeJRE.Components.BrowserComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JComponent;

public final class BrowserService {
    private static final Logger logger = LogManager.getLogger(BrowserService.class);
    private BrowserComponent browserComponent = null;

    public BrowserService(final String $projectId) {
        logger.info("BrowserService()");

        initialize();
    }

    public void initialize() {
        logger.info("initialize()");

        try {
            installExitHook();

            initializeBrowserComponent();
        } catch (Exception exception) {
            logger.fatal("Caught exception: " + exception.getMessage());
        }
    }

    public JComponent getViewComponent() {
        logger.trace("getViewComponent");

        return browserComponent;
    }

    private void initializeBrowserComponent() {
        logger.info("initializeMicrocoreComponent()");

        browserComponent = new BrowserComponent();
    }

    private void installExitHook() {
        logger.trace("installExitHook");

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                logger.info("Exiting");

                if (browserComponent != null) {
                    browserComponent.destroy();
                }
            }
        });
    }
}
