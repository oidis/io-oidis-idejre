/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Helpers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ActionListener;
import javax.swing.Timer;

public class PeriodicAction {
    private static final Logger logger = LogManager.getLogger(PeriodicAction.class);

    private Timer timer = null;

    public PeriodicAction(final int $millis, final ActionListener $action) {
        timer = new Timer($millis, $action);
        timer.setRepeats(true);
    }

    public void start() {
        logger.info("start");

        timer.start();
    }

    public void stop() {
        logger.info("stop");

        if (timer != null) {
            timer.stop();
        }
    }
}
