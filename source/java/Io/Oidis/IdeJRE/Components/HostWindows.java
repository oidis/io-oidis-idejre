/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Components;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef.HWND;

import java.awt.Graphics;

import Io.Oidis.IdeJRE.Components.Errors.InvalidHandle;
import Io.Oidis.IdeJRE.Components.Errors.NativeError;

public class HostWindows extends Host {
    private HWND handleHosted = null;
    private HWND hostedParent = null;
    private HWND handleSelf = null;

    @Override
    public void removeNotify() {
        this.release();
        super.removeNotify();
    }

    public HostWindows(final String $nativeWindowClassName) throws InvalidHandle {
        this(User32.INSTANCE.FindWindow($nativeWindowClassName, null));
    }

    public HostWindows(final HWND $nativeWindowHandle) throws InvalidHandle {
        handleHosted = $nativeWindowHandle;

        checkNativeHandleOfHosted();
    }

    private HostWindows() {
    }

    @Override
    public void release() throws NativeError {
        if (super.isHostingNativeWindow()) {
            if (User32.INSTANCE.SetParent(handleHosted, hostedParent) == null) {
                NativeError.throwWhenCodeIsNonZero("Failure when trying to call SetParent(...)", Kernel32.INSTANCE.GetLastError());
            }

            super.setIsHostingNativeWindow(false);
        }
    }

    @Override
    public void paint(final Graphics $graphics) throws NativeError {
        super.paint($graphics);

        host();

        if (!User32.INSTANCE.MoveWindow(handleHosted, 0, 0, getWidth(), getHeight(), true)) {
            NativeError.throwWhenCodeIsNonZero("Failure when trying to call MoveWindow(...)", Kernel32.INSTANCE.GetLastError());
        }
    }

    private void host() throws NativeError {
        if (!super.isHostingNativeWindow()) {
            loadNativeHandleOfSelf();

            if (handleHosted != null && handleSelf != null) {
                // setting WS_CHILD to the embedded window proved useless during empiric tests

                hostedParent = User32.INSTANCE.GetAncestor(handleHosted, User32.GA_PARENT);

                if (User32.INSTANCE.SetParent(handleHosted, handleSelf) == null) {
                    NativeError.throwWhenCodeIsNonZero("Failure when trying to call SetParent(...)", Kernel32.INSTANCE.GetLastError());
                }

                if (!User32.INSTANCE.ShowWindow(handleHosted, User32.INSTANCE.SW_SHOW)) {
                    NativeError.throwWhenCodeIsNonZero("Failure when trying to call ShowWindow(...)", Kernel32.INSTANCE.GetLastError());
                }

                super.setIsHostingNativeWindow(true);

                repaint();
            }
        }
    }

    private void checkNativeHandleOfHosted() throws NativeError, InvalidHandle {
        if (handleHosted == null) {
            throw new InvalidHandle("Native handle is null");
        }

        if (!User32.INSTANCE.IsWindow(handleHosted)) {
            NativeError.throwWhenCodeIsNonZero("Incorrect window handle, used IsWindow(...)", Kernel32.INSTANCE.GetLastError());
        }
    }

    private void loadNativeHandleOfSelf() {
        // need resolve own HWND everytime, because the Canvas component binds to different native handle
        // each time the underlying "addNotify()" is called

        handleSelf = new HWND(Native.getComponentPointer(this));
    }
}
