/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Components.Errors;

public final class NativeError extends RuntimeException {
    public static void throwWhenCodeIsNonZero(final String $message, final int $code) {
        if ($code != 0) {
            throw new NativeError($message + ", native error code: " + $code);
        }
    }

    private NativeError(final String $message) {
        super($message);
    }
}
