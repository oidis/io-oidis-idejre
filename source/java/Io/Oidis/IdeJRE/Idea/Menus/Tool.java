/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Idea.Menus;

import Io.Oidis.IdeJRE.Interfaces.ITool;
import com.intellij.openapi.actionSystem.AnAction;

public class Tool extends BaseItem implements ITool {
    private final String projectId;

    public Tool(final AnAction $nativeObject, final String $elementId, final String $projectId) {
        super($nativeObject, $elementId);
        projectId = $projectId;
    }

    @Override
    public void setHandler(final Runnable $runnable) {
        // todo : link with events manager based on native object id - this.getNativeObject()
    }
}
