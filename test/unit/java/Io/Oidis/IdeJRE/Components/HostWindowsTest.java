/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Components;

import Io.Oidis.IdeJRE.Components.Errors.InvalidHandle;
import Io.Oidis.IdeJRE.Components.Helper.WindowFactory;
import Io.Oidis.UnitTest;
import com.sun.jna.Native;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef.HWND;
import org.junit.Assert;

import javax.swing.*;
import java.awt.event.WindowEvent;

public class HostWindowsTest extends UnitTest {
    public void testConstructionWithInvalidWindowClassName() {
        try {
            new HostWindows("foo");
        } catch (InvalidHandle error) {
            Assert.assertFalse(error.getMessage().isEmpty());
            return;
        }

        Assert.fail("Expected InvalidHandle exception");
    }

    public void testConstructionWithValidHwnd() {
        constructAndDestructHost(new ConstructionCommand() {
            @Override
            public void execute(HWND handle) {
                new HostWindows(handle);
            }
        });
    }

    public void testConstructionWithValidClassName() {
        constructAndDestructHost(new ConstructionCommand() {
            @Override
            public void execute(HWND handle) {
                char[] buffer = new char[512];
                final int exitCode = User32.INSTANCE.GetClassName(handle, buffer, buffer.length);

                Assert.assertFalse("Invalid exit code when calling GetClassName(...): " + exitCode, exitCode == 0);

                final String className = Native.toString(buffer);

                Assert.assertFalse("Class name is empty", className.isEmpty());

                new HostWindows(className);
            }
        });
    }

    public void testDisplay() {
        displayHost(false);
    }

    public void testDisplayWithRelease() {
        displayHost(true);
    }

    private void displayHost(boolean release) {
        constructAndDestructHost(new ConstructionCommand() {
            @Override
            public void execute(HWND handle) {
                final Host host = new HostWindows(handle);

                final JFrame frame = new JFrame();
                frame.setSize(600, 400);
                frame.add(host);
                frame.setVisible(true);

                // after 1 second, close the JFrame that contains the Host itself (simulate user click)
                try {
                    Thread.sleep(1000);
                } catch (Exception error) {
                    Assert.fail("Encountered exception during sleep/wait");
                }

                if (release) {
                    host.release();
                }

                frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
            }
        });
    }

    private void constructAndDestructHost(ConstructionCommand command) {
        JFrame window = null;

        try
        {
            window = WindowFactory.CreateWin32WindowAndWaitUntilVisible();
            final HWND handle = new HWND(Native.getComponentPointer(window));

            Assert.assertTrue(handle != null);

            command.execute(handle);
        }
        catch (Exception error)
        {
            Assert.fail("Error intercepted: " + error.getMessage());
        }

        // window is disposed automatically when Host is destroyed
    }

    private interface ConstructionCommand
    {
        void execute(HWND handle);
    }
}
