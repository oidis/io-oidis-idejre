/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Components;

import java.awt.Canvas;
import java.awt.Graphics;
import java.util.concurrent.atomic.AtomicBoolean;

import Io.Oidis.IdeJRE.Components.Errors.NativeError;

public abstract class Host extends Canvas {
    private AtomicBoolean isHostingNativeWindow = new AtomicBoolean(false);

    public abstract void release() throws NativeError;

    @Override
    public void paint(final Graphics $graphics) throws NativeError {
        super.paint($graphics);
    }

    protected boolean isHostingNativeWindow() {
        return isHostingNativeWindow.get();
    }

    protected void setIsHostingNativeWindow(final boolean $value) {
        isHostingNativeWindow.set($value);
    }
}
