/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Idea.Factories;

import Io.Oidis.IdeJRE.Idea.Actions.Action;
import Io.Oidis.IdeJRE.Idea.Actions.ActionGroup;
import Io.Oidis.IdeJRE.Idea.Activator;
import Io.Oidis.IdeJRE.Idea.Menus.Menu;
import Io.Oidis.IdeJRE.Idea.Menus.MenuItem;
import Io.Oidis.IdeJRE.Idea.Menus.Tool;
import Io.Oidis.IdeJRE.Idea.Views.View;
import Io.Oidis.IdeJRE.Interfaces.IItem;
import Io.Oidis.IdeJRE.Interfaces.IMenu;
import Io.Oidis.IdeJRE.Interfaces.IPartFactory;
import Io.Oidis.IdeJRE.Interfaces.ITool;
import Io.Oidis.IdeJRE.Interfaces.IView;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class IdeaPartFactory implements IPartFactory {

    private static Map<String, Project> projectPerProjectId;
    private static Map<String, IdeaPartFactory> factoryPerProjectId;
    private static Map<String, String> labelPerElementId; // idea uses view ids for labels
    private static List<String> definedParts = new ArrayList<>();
    private final Project project;

    private IdeaPartFactory(final Project $project) {
        project = $project;
    }

    public static IdeaPartFactory getInstance(final String $projectId) {
        if (factoryPerProjectId == null) {
            factoryPerProjectId = new HashMap<>();
        }
        if (!factoryPerProjectId.containsKey($projectId)) {
            Project project = projectPerProjectId.get($projectId);
            factoryPerProjectId.put($projectId, new IdeaPartFactory(project));
        }
        return factoryPerProjectId.get($projectId);
    }

    public static void RegisterProject(final Project $project) {
        if (projectPerProjectId == null) {
            projectPerProjectId = new HashMap<>();
        }
        projectPerProjectId.put($project.getName(), $project);
    }

    public static void RegisterDefinedPart(final String $partId) { //todo : split into different types of parts by enum
        definedParts.add($partId);
    }

    public static void UnregisterProject(final Project $project) {
        projectPerProjectId.remove($project.getName());
    }

    public static Project getProject(final String $projectId) {
        return projectPerProjectId.get($projectId);
    }

    public static void RegisterView(final String $elementId, final String $label) {
        if (labelPerElementId == null) {
            labelPerElementId = new HashMap<>();
        }
        labelPerElementId.put($elementId, $label);
    }

    @Override
    public IMenu getDefinedMenu(final String $menuId) {
        String elementId = Activator.getPluginId() + ".Menus." + $menuId;
        if (definedParts.contains(elementId)) {
            ActionGroup menu = (ActionGroup) ActionManager.getInstance()
                    .getAction(elementId);
            return new Menu(menu, elementId);
        }
        return null;
    }

    @Override
    public IMenu createMenu(final String $menuId) {
        String elementId = Activator.getPluginId() + ".Menus." + $menuId;
        ActionGroup actionGroup = (ActionGroup) ActionManager.getInstance().getAction(elementId);
        if (actionGroup == null) {
            actionGroup = new ActionGroup();
            actionGroup.setProject(project.getName());
            actionGroup.setPopup(true);
            ActionManager.getInstance().registerAction(elementId, actionGroup);
        }
        return new Menu(actionGroup, elementId);

    }

    @Override
    public IItem createMenuItem(final String $menuItemId) {
        String elementId = Activator.getPluginId() + ".MenuItems." + $menuItemId;
        Action action = (Action) ActionManager.getInstance().getAction(elementId);
        if (action == null) {
            action = new Action() {
                @Override
                public void actionPerformed(final AnActionEvent $anActionEvent) {
                    // todo : invoke eventsmanager with id of element as owner also needs to be linked with project
                    // => $anActionEvent.getProject(), so only one action needs to be inside menu
                }
            };
            action.setProject(project.getName());
            ActionManager.getInstance().registerAction(elementId, action);
        }
        return new MenuItem(action, elementId, project.getName());
    }

    @Override
    public ITool getDefinedTool(final String $toolId) {
        String elementId = Activator.getPluginId() + ".Tools." + project.getName() + "." + $toolId;
        if (definedParts.contains(elementId)) {
            Action action = (Action) ActionManager.getInstance().getAction(elementId);
            if (action == null) {
                action = new Action() {
                    @Override
                    public void actionPerformed(final AnActionEvent $anActionEvent) {
                        // todo : invoke eventsmanager with id of element as owner also needs to be linked with project
                        // => $anActionEvent.getProject(), so only one action needs to be inside menu
                    }
                };
                action.setProject(project.getName());
                ActionManager.getInstance().registerAction(elementId, action);
            }
            return new Tool(action, elementId, project.getName());
        }
        return null;
    }

    @Override
    public IView getDefinedView(final String $viewId) {
        String elementId = Activator.getPluginId() + ".Views." + project.getName() + "." + $viewId;
        return new View(labelPerElementId.get(elementId), project.getName());
    }
}
