/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Eclipse.Menus;

import Io.Oidis.IdeJRE.Interfaces.IHandledItem;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;

public class MenuItem extends BaseItem implements IHandledItem {
    public MenuItem(final MMenuElement $nativeObject) {
        super($nativeObject);
    }

    @Override
    public void setHandler(final Runnable $runnable) {
        // todo : link with events manager based on native object id - this.getNativeObject()
    }
}
