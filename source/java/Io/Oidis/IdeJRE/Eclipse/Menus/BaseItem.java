/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Eclipse.Menus;

import Io.Oidis.IdeJRE.Loader;
import org.eclipse.e4.ui.model.application.ui.MUILabel;

public class BaseItem {

    private final MUILabel nativeObject;

    public BaseItem(final MUILabel $nativeObject) {
        nativeObject = $nativeObject;
    }

    public MUILabel getNativeObject() {
        return nativeObject;
    }

    public String getLabel() {
        return nativeObject.getLabel();
    }

    public void setLabel(final String $label) {
        nativeObject.setLabel($label);
    }

    public void setIconPath(final String $iconPath) {
        nativeObject.setIconURI("platform:/plugin/" + Loader.getPluginRuntime().getPluginId() + "/" + $iconPath);
    }
}
