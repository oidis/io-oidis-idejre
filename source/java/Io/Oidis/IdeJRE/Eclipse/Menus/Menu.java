/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Eclipse.Menus;

import Io.Oidis.IdeJRE.Interfaces.IMenu;
import Io.Oidis.IdeJRE.Interfaces.IItem;
import org.eclipse.e4.ui.model.application.ui.menu.MMenu;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;

import java.util.ArrayList;
import java.util.List;

public class Menu extends BaseItem implements IMenu {

    private List<IItem> items;

    public Menu(final MMenu $nativeObject) {
        super($nativeObject);
        items = new ArrayList<>();
    }

    @Override
    public void addItem(final IItem $item) {
        if (!items.contains($item)) {
            items.add($item);
            if ($item instanceof BaseItem) {
                ((MMenu) this.getNativeObject()).getChildren().add((MMenuElement) ((BaseItem) $item).getNativeObject());
            }
        }
    }
}
