/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE;

import Io.Oidis.IdeJRE.Interfaces.IItem;
import Io.Oidis.IdeJRE.Interfaces.IMenu;
import Io.Oidis.IdeJRE.Interfaces.IPartFactory;
import Io.Oidis.IdeJRE.Interfaces.IPluginRuntime;
import Io.Oidis.IdeJRE.Interfaces.IView;
import Io.Oidis.IdeJRE.Services.BrowserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class Loader { // checkstyle:disable-line

    private static final Logger logger = LogManager.getLogger(Loader.class);
    private static IPluginRuntime pluginRuntime;

    public static void Load(final IPluginRuntime $pluginRuntime, final String $projectId) throws IOException {
        logger.trace("Load()");

        if (pluginRuntime == null) {
            pluginRuntime = $pluginRuntime;
        }

        IPartFactory factory = pluginRuntime.getPartFactory($projectId);

        IMenu dynamicMenu = factory.getDefinedMenu("DynamicMenu");
        IItem menuItem = factory.createMenuItem("menuItemId");
        IMenu submenu = factory.createMenu("subMenuId");

        submenu.setLabel("Submenu");
        submenu.addItem(menuItem);
        menuItem.setLabel("Embedded Browser");
        menuItem.setIconPath("resource/icon.png");
        dynamicMenu.addItem(submenu);

        final IView browserView = factory.getDefinedView("WuiView");
        BrowserService browserService = new BrowserService($projectId);
        browserView.setContent(browserService.getViewComponent());
    }

    public static IPluginRuntime getPluginRuntime() {
        return pluginRuntime;
    }
}
