# com-wui-framework-idejre v2019.2.0

> WUI Framework runtime environment for Java-based IDEs.

## Requirements

This library does not have any special requirements, but it depends on the 
[WUI Builder](https://gitlab.com/oidis/io-oidis-builder). See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the
[WUI Builder](https://gitlab.com/oidis/io-oidis-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [Javadoc](http://docs.oracle.com/javase/8/docs/technotes/tools/windows/javadoc.html) 
from the Java source by running the `wui docs` command from the {projectRoot} folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2019.1.0
Usage of microcore project instead of JXBrowser.
### v1.2.0
Update of SCR and history ordering.
### v1.1.1
Added support for configs in JSONP. Usage of eclipse 4.x API. Added advanced hooks into targed IDEs.
### v1.1.0
Added support for bi-directional communication with browser. Implemented basic IDE Handler. Structure clean up.
### v1.0.2
Added support for IDEA. Tests updated.
### v1.0.1
Structure clean up.
### v1.0.0
Initial release.

## License

This software is owned or controlled by Oidis. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2017-2019 [NXP](http://nxp.com/)
Copyright (c) 2019 [Oidis](https://www.oidis.org/)
