/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Eclipse.Handlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuItem;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;

public class Handler {
    private static final Logger logger = LogManager.getLogger(Handler.class);

    @Execute
    public void execute(@Optional final MToolItem $toolItem, @Optional final MMenuItem $menuItem) {
        logger.trace("execute: " + $toolItem + "; " + $menuItem);

        //todo : invoke eventsmanager with element id
        if ($menuItem != null) {
            System.out.println($menuItem.getElementId());
        } else if ($toolItem != null) {
            System.out.println($toolItem.getElementId());
        }
    }
}
