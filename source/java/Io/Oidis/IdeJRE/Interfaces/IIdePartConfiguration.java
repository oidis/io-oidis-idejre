/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Interfaces;

public class IIdePartConfiguration {
    /* checkstyle:disable: JavadocVariable|VisibilityModifier|MemberName */
    public String id;
    public String label;
    public String icon;
    /* checkstyle:enable */
}
