/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Components;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JComponent;
import java.awt.Graphics;

public class BrowserComponent extends JComponent {
    private static final Logger logger = LogManager.getLogger(BrowserComponent.class);

    private Host host = null;

    public BrowserComponent() {
        logger.info("BrowserComponent()");

        host = new HostWindows("chromiumRE");

        add(host);
    }

    public void destroy() {
        logger.info("destroy");

        host.setVisible(false);
    }

    @Override
    public void paintComponent(final Graphics $graphics) {
        logger.trace("paintComponent: " + this);

        super.paintComponent($graphics);

        if (host != null) {
            host.setSize(getSize());
        }
    }
}
