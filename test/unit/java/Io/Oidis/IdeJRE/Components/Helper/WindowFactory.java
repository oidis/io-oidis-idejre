/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.IdeJRE.Components.Helper;

import javax.swing.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

public final class WindowFactory {
    private static final Object lock = new Object();
    private static boolean windowVisible = false;

    public static JFrame CreateWin32WindowAndWaitUntilVisible() throws InterruptedException {
        // Creating of win32 window using JNA proved very difficult (weird errors), so instead create a JFrame that also has own HWND

        ComponentListener listener = new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent event) {
                synchronized (lock) {
                    windowVisible = true;
                    lock.notify();
                }
            }
        };

        JFrame frame = new JFrame("");
        frame.setUndecorated(true);
        frame.addComponentListener(listener);
        frame.setVisible(true);

        waitUntilVisible();

        return frame;
    }

    private static void waitUntilVisible() throws InterruptedException {
        synchronized (lock) {
            while(!windowVisible) {
                lock.wait();
            }
        }
    }
}
